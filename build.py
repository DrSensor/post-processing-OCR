#!/usr/bin/env python

from pybuilder.core import use_plugin, init, Author

use_plugin('python.core')
# use_plugin('python.unittest')
# use_plugin('python.coverage')
# use_plugin('python.integrationtest')
use_plugin('python.install_dependencies')
use_plugin('python.flake8')
use_plugin('python.distutils')
# use_plugin('python.pycharm')

name = 'post-processing-OCR'
license = 'Apache License, Version 2.0'
# authors = [Author('Michael Gruber', 'aelgru@gmail.com'),
        #    Author('Alexander Metzner', 'halimath.wilanthaou@gmail.com')]
# summary = 'Hello world application for flask.'
# url = 'https://github.com/pycletic/flask-example'
# version = '0.1.2'


default_task = ['analyze']


@init
def set_properties (project):
    # project.build_depends_on_requirements('requirements.txt')
    # project.set_property("dir_dist_scripts", 'scripts')
    # project.set_property("dir_dist_scripts", 'configs')
    project.depends_on_requirements('requirements.txt')
