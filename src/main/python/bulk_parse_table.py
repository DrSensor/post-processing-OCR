#!/usr/bin/env python

# TODO : add file name

from ConfigParser import ConfigParser, NoOptionError, NoSectionError
from os.path import basename
from glob import glob
from tqdm import tqdm
import pandas as pd
import argparse
import signal
import json
import sys
# import csv
import os

from cleansing import remove_gibberish, find_price, find_date
from parse_html import ParseTable


# TODO: autodefine from *.ini file
fieldnames_cell = ['Invoice date', 'Invoice Number', 'Total Value', 'Shop Name', 'Shop Address']
fieldnames_column = ['Product Code', 'Product Description', 'Unit', 'Price', 'Discount']


def cleansing_rule(df):
    brutal = {}
    for column in df:
        if column == "File Name":
            purify = lambda x: x
        elif column in ['Price','Discount','Total Value']:
            purify = lambda x: find_price(x) if type(x) is str else None
        elif column in 'Invoice date':
            purify = lambda x: find_date(x) if type(x) is str else None
        else:
            purify = lambda x: remove_gibberish(x) if type(x) is str else None
            if column in 'Product Description':
                purify = lambda x: drop_nonsense(x)
                purify = lambda x: None if x in 'NAMA BARANG' else x
        brutal.update({
            column: df[column].map(purify).dropna().tolist()
        })
    df = df.from_dict({k : pd.Series(v) for k, v in brutal.iteritems()})
    return df


def parse_saveas_tsv(save_as, process_file):
    # Parse and convert as json like then store it as DataFrame
    pt = ParseTable(process_file, config)
    results = pt.parse_as_json(fieldnames_cell, fieldnames_column)
    df = pd.DataFrame(results)
    df = cleansing_rule(df)

    # if both of cols_of_interest is None then drop
    cols_of_interest = ['Product Description', 'Unit', 'Price', 'Discount']
    df = df[(df[cols_of_interest]).any(axis=1)]

    df = df.set_index("File Name")
    print_header = True if idf == 0 else False
    df.to_csv(save_as, mode='a', sep='\t', header=print_header)
    # for res in results:
        # writer.writerow(res)
    save_as.flush()


if __name__ == '__main__':
    def exit_gracefully(signum, frame):
        # restore the original signal handler as otherwise evil things will happen
        # in raw_input when CTRL+C is pressed, and our signal handler is not re-entrant
        signal.signal(signal.SIGINT, original_sigint)

        try:
            if raw_input("\nReally quit? (y/n)> ").lower().startswith('y'):
                sys.exit(1)

        except KeyboardInterrupt:
            print("Ok ok, quitting")
            # ps.browser.quit()
            sys.exit(1)

            # restore the exit gracefully handler here
            signal.signal(signal.SIGINT, exit_gracefully)

    # store the original SIGINT handler
    original_sigint = signal.getsignal(signal.SIGINT)
    signal.signal(signal.SIGINT, exit_gracefully)

    # Parse Argument
    parser = argparse.ArgumentParser(description='Parse html table from abyy finereader result')
    parser.add_argument('folder', type=str,
                            help='folder of the file (.htm)')
    parser.add_argument('-o', '--output', type=str, default='result.tsv',
                            help='output of the file (.tsv)')
    parser.add_argument('-c', '--config', type=str, default='config.ini',
                            help='Configuration file that want to be load (.ini .cfg)')
    args = parser.parse_args()

    # read configuration files
    config = ConfigParser()
    config.read(args.config)

    ########################## MAIN PROGRAM ###########################
    files = glob(args.folder+'/*.htm')

    with open(args.output, 'wb') as csvfile:
        # writer = csv.DictWriter(csvfile, delimiter='\t', fieldnames=fieldnames)
        # writer.writeheader()

        for idf,path_file in enumerate(tqdm(files)):
            parse_saveas_tsv(csvfile, path_file)
